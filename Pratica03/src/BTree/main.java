/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BTree;

import Item.Item;

/**
 *
 * @author messiah
 */
public class main {

    public static void main(String[] args) {
        /** Para cada valor de caso de teste (10.000, 20.000, 30.000, 40.000, 50.000,
         * 60.000, 70.000, 80.000, 90.000, 10.000) foi instanciada um novo objeto BTree.
         */
        for(int k = 0; k < 10; k++){
            System.out.println("\n\nCASO DE TESTE: "+k+"\n");
               
            //Gerar o árvore B a partir de n elementos ORDENADOS de ordem 2.
            System.out.println("Gerar árvores a partir de n elementos ORDENADOS de ordem 2:");
            for(int n = 10000; n <= 100000; n += 10000){
                BTree bt = new BTree(2);
                for(int i = 0; i < n; i++) bt.insert(new Item(i));
                bt.search(new Item(n));
                System.out.println("\tNúmero de páginas visitadas: "+bt.number_of_pages+" Com n:"+(n));
            }
            
            //Gerar o árvore B a partir de n elementos ORDENADOS de ordem 4.
            System.out.println("Gerar árvores a partir de n elementos ORDENADOS de ordem 4:");
            for(int n = 10000; n <= 100000; n += 10000){
                BTree bt = new BTree(4);
                for(int i = 0; i < n; i++) bt.insert(new Item(i));
                bt.search(new Item(n));
                System.out.println("\tNúmero de páginas visitadas: "+bt.number_of_pages+" Com n:"+(n));
            }
            
            //Gerar o árvore B a partir de n elementos ORDENADOS de ordem 6.
            System.out.println("Gerar árvores a partir de n elementos ORDENADOS de ordem 6:");
            for(int n = 10000; n <= 100000; n += 10000){
                BTree bt = new BTree(6);
                for(int i = 0; i < n; i++) bt.insert(new Item(i));
                bt.search(new Item(n));
                System.out.println("\tNúmero de páginas visitadas: "+bt.number_of_pages+" Com n:"+(n));
            }
        } 
    }
    
}
