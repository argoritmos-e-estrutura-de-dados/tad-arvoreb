package BTree;

import Item.Item;

/** @author messiah */
public class BTree {
    private static class Page{
        /** Número de itens na página. */
        int n;
        /** Vetor contendo os registros da página de tamanho 2m = mm. */
        Item r[];
        /** Vetor de objetos para as páginas a um nível abaixo de tamanho 2m+1. */
        Page p[];
        public Page(int mm){
            this.n = 0;
            r = new Item[mm];
            this.p = new Page[mm+1];
        }
    }
    /** Raíz da árvore B. */
    private Page root;
    /** Quantidade mínima e máxima em cada página. */
    private int m, mm;
    /** Número de comparações na pesquisa de um item it. */
    int number_of_pages;
    
    BTree(int m){
        this.m = m;
        this.mm = 2*m;
        this.root = null;
        number_of_pages = 0;
    }
    
    /** Método auxiliar para inserção do Item it na página p_right. */
    private void insertInPage(Page p, Item it, Page p_right){
        /** k recebe a posição mais válida à direita. */
        int k = p.n - 1;
        /** Desloca-se os elementos de r e p para a direita até que a posição
         correta de inserção seja encontrada. */ 
        while((k >= 0) && (it.compare(p.r[k]) < 0)){
            p.r[k+1] = p.r[k];
            p.p[k+2] = p.p[k+1];
            k--;
        }
        /** O Item 'it' e a page 'p_right' são inseridos na página corrente. */
        p.r[k+1] = it;
        p.p[k+2] = p_right;
        p.n++;
    }
    
    public void insert(Item it){
        Item it_insert[] = new Item[1]; /** Indica o Item que vai sr inserido, obido do método insere. */
        boolean up[] = new boolean[1]; /** Informa que um registro mudou para a página superior proveniente de uma inserção. */
        Page p_insert = this.insert(it, this.root, it_insert, up);
        /** Deposi da inserçõ do elemento 'it' a partir de 'this.root' é verificado
         se houve crescimento ou não na raiz. */
        if(up[0]){
            /** Caso afirmativo, uma nova pagina é criada e atribuída à raiz. */
            Page p_temp = new Page(this.mm);
            p_temp.r[0] = it_insert[0];
            p_temp.p[0] = this.root;
            p_temp.p[1] = p_insert;
            this.root = p_temp;
            this.root.n++;
        /** Vai ocorrer na última chamada recursiva, quando o deslocamento chega a raiz. */
        }else this.root = p_insert; 
    }
    
    private Page insert(Item it, Page p, Item[] it_insert, boolean[] up){
        Page p_insert = null;
        /** Caso 'p == null', a página onde o registro deve ser inserido foi encontrado
         e as variáveis 'up' e it_insert são atualizados. */
        if(p == null){
            up[0] = true;
            it_insert[0] = it;
        }else{
            int i = 0;
            /** A partir da esquerda, 'i' é posicioando no primeiro elemento que
             seja maior ou igual ao registro a ser inserido. */
            while((i < p.n-1) && (it.compare(p.r[i]) > 0)) i++;
            /** Se o registro já existe. */
            if(it.compare(p.r[i]) == 0){
                up[0] = false;
            }else{
                /** Verifica se a próxima página a ser pesquisada deve ser a da 
                 direita ou a da esquerda; Essa verificação é realizada pois i 
                 pode estar posicioanda na última posição de 'r[]'; invoca o 
                 insere novamante. */
                if(it.compare(p.r[i]) > 0) i++;
                p_insert = this.insert(it, p.p[i], it_insert, up);
                if(up[0])
                    /** Ápos a chamada recursiva it_insert[0] recebe it. */
                    if(p.n < this.mm){
                        /** Caso haja espaço na página realiza-se a inserção do elemento 'it'. */
                        this.insertInPage(p, it_insert[0], p_insert);
                        up[0] = false;
                        p_insert = p;
                    /** Caso não haja espaço na página e essa tera que ser dividida. */
                    }else{
                        /** É criada uma nova página p_temp. */
                        Page p_temp = new Page(this.mm);
                        p_temp.p[0] = null;
                        /** Verifica se o novo registro deve ficar na nova página
                         * p_temp ou na págian atual p. */
                        if(i <= this.m){
                            this.insertInPage(p_temp, p.r[this.mm-1], p.p[this.mm]);
                            p.n--;
                            this.insertInPage(p, it_insert[0], p_insert);
                        }else this.insertInPage(p_temp, it_insert[0], p_insert);
                        /** Transfere a metade direita da página atual p para a 
                         nova página p_temp. */
                        for(int j = this.m+1; j < this.mm; j++){
                            this.insertInPage(p_temp, p.r[j], p.p[j+1]);
                            p.p[j+1] = null;
                        }
                        p.n = this.m;
                        p_temp.p[0] = p.p[this.m+1];
                        /** Atribui o registro do meio à it_insert e tribui o objeto
                         p_temp à p_insert. */
                        it_insert[0] = p.r[this.m];
                        p_insert = p_temp;
                    }
            }
        }
        /** Retorna a árvore nova ou atual. */
        return (up[0] ? p_insert : p);
    }    
    
    public void search(Item it){
        this.search(it, this.root);
    }
    /** Pesquisar o item 'it' na árvore. */
    private Item search(Item it, Page p){
        if(p == null) return null;
        else{
            int i = 0;
            /** Verifica se a posição atual 'i' é válida e se o registro procurado
             é maior que o atual e incrementa a posição. */
            while((i < p.n-1) && (it.compare(p.r[i]) > 0)) i++;
            /** Encontra o menor elemento que seja maior ou igual que o elemento procurado. */
            /** Se for igual retorna o elemento. */
            if(it.compare(p.r[i]) == 0)
                return p.r[i];
            /** Se o elemento for menor que a posição atual. */
            else if(it.compare(p.r[i]) < 0){
                number_of_pages++;
               /** Então chama recursivamente a busca na sub ávorea esquerda. */
               return search(it, p.p[i]);
            }
            /** Se não chama recursivamente a busca na subárvore direita. */
            else{
                number_of_pages++;
                return search(it, p.p[i+1]);
            }
        }   
    }    
}
